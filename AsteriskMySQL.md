## MonACC con Base de Datos en MySQL

Si se opta por querer manejar el CDR en la base de datos de MySQL, puedes seguir los siguientes pasos.

1. #### Instalar dependecias para Asterisk

Es necesario tener las dependencias resultas para que Asterisk pueda hacer uso de la conexión a la base de datos. En esta ocasion se indicara hacerlo a traves de ODBC.

Primero hay que instalar la dependencias de librerias y herramientas, para este ejemplo nos basaremos en la distribucion CentOS 7 64 bits. Por lo que ejecutaremos:

```bash
$ yum install unixODBC.x86_64 unixODBC-devel.x86_64 mysql-connector-odbc.x86_64 libtool-ltdl-devel.x86_64 -y
```

Posiblemente sera necesario recompilar Asterisk si se instalo desde las fuentes, si se instalo desde los binarios posiblemente solo sera necesario revisar si se tiene los modulos:

- cdr_adaptive_odbc.so
- res_odbc.so

Los cuales se encargaran de cargar las configuracion y realizar las conexiones hacia la base de datos, asi como insertar los registros cada vez que se genere el CDR de una llamada.

2. #### Configurar ODBC en Linux

Es necesario configurar ODBC a nivel de Linux para que la configuracion en Asterisk pueda utilizar. Para esto es necesario revisar/configurar:

- Revisar */etc/odbcinst.ini*, del cual hay que confirmar que se tenga:

```
# Driver from the mysql-connector-odbc package
# Setup from the unixODBC package
[MySQL]
Description     = ODBC for MySQL
Driver          = /usr/lib/libmyodbc5.so
Setup           = /usr/lib/libodbcmyS.so
Driver64        = /usr/lib64/libmyodbc5.so
Setup64         = /usr/lib64/libodbcmyS.so
FileUsage       = 1
```

- Configurar */etc/odbc.ini*, en la cual se dará de alta el ODBC que utilizará Asterisk, por lo que deberiamos de dejar algo como:

```
[Asterisk]
Driver=MySQL
Server=localhost
Port=3306
#Socket=/var/lib/mysql/mysql.sock
Database=asterisk
User=unusuario
Password=super.secreto
```

Teniendo lo anterior hay que instalar el origen recien creado, para esto podemos ejecutar:

```bash
$ odbcinst -i -s -f /etc/odbc.ini
```

3. #### Configurar en Asterisk el ODBC

Para poder utilizar lo antes configurado es necesario configurar en Asterisk, para esto editamos el archivo */etc/asterisk/res_odbc.conf* y escribimos:

```vim
[monacc]
enabled => yes
dsn => Asterisk
username => unusuario
password => super.secreto
pre-connect => yes
sanitysql => select 1
```

Posterior es necesario editar el archivo */etc/asterisk/cdr_adaptive_odbc.conf*, en el cual se define como se ha de conectar y la tabla (y campos) que ha de utilizar, para el ejemplo tenemos:

```vim
[monacc]
connection=monacc
table=cdr
alias start = calldate
alias answer = answerdate
alias end = enddate
```

Por ultimo es necesario (si no se tiene) activar el manejo de CDR en Asterisk, esto lo podemos validar/configurar en el archivo */etc/asterisk/cdr.conf*, se debe tener:

```vim
[general]
enable=yes

[custom]
; We log the unique ID as it can be useful for troubleshooting any issues
; that arise.
loguniqueid=yes
```

4. #### Crear la base de datos

Para esto se tiene un archivo con la definicion de la base de datos, el cual hay que cargar en MySQL. Es se encuentra en el directorio *sql* con el nombre de *monacc.sql*. Para instalar puedes ejecutar:

```bash
$ mysql -u root -p < sql/monacc.sql
```

Lo anterior debe crear la base de datos, crear la tabla y la vista necesaria.

5. #### Activar en Asterisk

Teniendo las configuraciones hechas hay que recargar en Asterisk para que realiza la conexion y esta pueda comenzar a registrar en al base de datos,para lo anterior se puede ejecutar:

```bash
$ rasterisk -x "core reload"
```

Y para confirmar que la conexion esta hecha podemos confirmar ejecutando:

```bash
$ rasterisk -x "odbc show"

ODBC DSN Settings
-----------------

  Name:   monacc
  DSN:    monacc
    Last connection attempt: 1969-12-31 18:00:00
    Number of active connections: 1 (out of 1)
    Logging: Disabled
```

Con lo anterior se confirma que la conexion se encuentra hecha. Restaría revisar que la tabla se empiecen a tener registros.