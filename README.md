# Servicio MonACC
## Monitor de Asterisk Call Center (Colas)

Servicio que permite generar un servicio Web para administrar un monitoreo
sobre un servicio [Asterisk](www.asterisk.org), el cual permite visualizar las actividades en
la(s) cola(s) que este tenga, permitiendo revisar en tiempo real su comportamiento
a traves de conexión **WebSocket** ([Socket.IO](https://socket.io))

#### Especificaciones

El servicio debe cumplir con las siguientes especificaciones

- Asterisk v13+, la aplicacion fue pensada para esta serie (probada y desarrollada con la 13.30.0), la misma se puede descargar [aquí](https://downloads.asterisk.org/pub/telephony/asterisk/asterisk-13.33.0.tar.gz)
- Node.JS v12+, la version especifica con la que desarrollo fue la v12.16.3
- MySQL 5+, es opcional si se piensa utilizar el CDR, para ver la configuracion probada ver el archivo AsteriskMySQL.md


#### Requisitos

Es necesario aplicar configuraciones particulares, dependiendo de tu ambiente (Instalacion de Asterisk y Base de datos, si quiere utilizar).
Básicamente son 2 configuraciones que se tiene que hacer en la herramienta

1. Configurar las credenciales de acceso a AMI

La herramienta trabaja conexiones tipo AMI \(Asterisk Manager Interface\), con la cual procesa los eventos y realiza peticiones.
Para esto es necesario configurar en el archivo */etc/asterisk/manager.conf* \(si tienes la configuracion por default\)

Para lo anterior deberas de crear un usuario, por ejemplo:

```vim
; Es necesario activar el manager para en el contexto general
[general]
enabled = yes
port = 5038
; si quieren escuchar en todas la IP's configuradas
bindaddr = 0.0.0.0

[monacc]
deny=0.0.0.0/0.0.0.0
permit=127.0.0.1/255.255.255.0
secret=M0n4Cc.2o20
read=all
write=all
```

Una vez activado y configurado el usuario, hay que recargar la configuracio en Asterisk, se puede ejecutar desde consola linux:

```bash
$ rasterisk -x "manager reload"
```

Posterior es necesario ir a la herramienta y configurar las credenciales, esto se realiza en el archivo *index.js* en la linea 79, dejando:

```javascript
var ami = new Asterisk(5038, 'localhost', 'monacc', 'M0n4Cc.2o20', true);
```

2. Configurar acceso a base de datos.

En caso de querer registrar el CDR en la base de MySQL, hay que indicarle a la herramienta los datos de conexion, esto se realiza en el archivo *module/cdr.js* en la linea 18, se vera algo como:

```javascript
//Datos de conexion a la base de datos
const config = {
	host: "localhost",
	user: "unusuario",
	password: "super.secreto",
	database: "asterisk"
};
```
Definir los accesos como se desee, la base de datos se esta dejando como *asterisk* pero se puede definir como se desee.

#### Instalar dependencias

Al estar desarrollado en Node.JS, se necesitan modulos para trabajar con el mismo, 
para instalar esta dependencias solo hay que ejecutar el siguiente comando en la carpeta
donde deseas tener el servicio:

```bash
$ npm install
```

Una vez resultas todas las dependencias se procede a iniciar el servicio.

#### Iniciar Servicio

Dependiendo de la ruta donde quieras tener el servicio, debes ejecutar:

```bash
$ PORT 3000 node index.js &
```

Lo anterior enviará el servicio en segundo plano, escuchando en el puerto 3000. Si no se define el puerto, por default inicia en el 3000.

**NOTA**

El servicio sigue en desarrollo y se estarán agregando funcionalidades, si se tiene alguna sugerencia, son bienvenidas.
