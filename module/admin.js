const mysql = require('mysql');
const { dbase } = require('../config');

const Admin = {
    queues: function (action, filter) {
        return new Promise((resolve, reject) => {
            let statemens = {
                post: "INSERT INTO queues SET ?",
                get: "SELECT * FROM queues",
                put: "UPDATE queues SET ? WHERE ?",
                delete: "DELETE FROM queues WHERE ?"
            };
    
            if (!(action in statemens))
                return reject({code: 400, message: "Action not implemented"});
            
            let cnn = mysql.createConnection(dbase);
            //TODO: revisar el filtro cuando es 'read' para poner el 'where'
            cnn.connect((err) => {
                if (err)
                    return reject({code: 500, message: err.message});
                
                let query = statemens[action];
                cnn.query(query, filter, function (err, rows) {
                    cnn.destroy();

                    if (err)
                        return reject({code: 500, message: err.messsage});
                    
                    return resolve(rows);
                });
            });
        });
    },
    devices: function (action, filter) {
        return new Promise((resolve, reject) => {
            let statemens = {
                post: "",
                get: "SELECT CONCAT_WS('/', `protocol`, `extension`) AS `device` FROM `phones` WHERE `active` = 'Y' AND `protocol` IN ('SIP','IAX2')",
                put: "",
                delete: ""
            };

            if (!(action in statemens))
                return reject({code: 400, message: "Action not implemented"});

            let cnn = mysql.createConnection(dbase);
            cnn.connect((err) => {
                if (err)
                    return reject({code: 500, message: err.message});
                
                let query = statemens[action];
                cnn.query(query, filter, (err, rows) => {
                    cnn.destroy();

                    if (err)
                        return reject({code: 500, message: err.message});
                    
                    return resolve(rows);
                });
            });
        });
    }
};

module.exports = {
    main: function (module, method, params) {
        if (!(module in Admin))
            return Promise.reject({code: 404, message: "Module not found"});
        
        return Admin[module](method, params);
    }
};