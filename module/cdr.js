/**
 * @script cdr.js
 * @author Saul Uribe <suribe@tico.com.mx>, <uribe.saul@yahoo.com.mx>
 * @date 02/May/2020
 * 
 * Modulo para la administracion de los registros de llamadas (CDR)
 * generados en Asterisk, permitiendo busqueda de fechas en particular
 * devolviendo la lista, recupera una grabacion de una llamada en caso
 * de tener disponible y recuperar la lista en formato CSV
*/

const fs = require('fs');
const mysql = require('mysql');

//Ruta de las grabaciones
const monitorPath = "/var/spool/asterisk/monitor";
//Datos de conexion a la base de datos
const { dbase } = require('../config');

/**
 * Determina si el objecto esta vacio
*/
Object.prototype.isEmpty = function () {
	return Object.keys(this).length === 0;
};

/**
 * Determina si el archivo dado (con posible ruta)
 * existe o no, si es tipo archivo
 * 
 * @param {String} file Nombre del archivo (con ruta)
 * @return {Promise} Retorno de promesa de ejecucion
*/
function checkFile (file) {
	return new Promise((resolve) => {
		fs.stat(file, (err, st) => {
			let isfile = (err ? false : st.isFile());
			
			if (err) 
				console.error(err.message);
			
			resolve(isfile);
		});
	});
}

/**
 * Define el nombre del archivo de grabacion a dar a un registro
 * determinado
 * 
 * @param {String} uniqueid Identificador unico generado por Asterisk
 * @return {Promise} Retorno de promesa de ejecucion
*/
function getRecordName (uniqueid) {
	return new Promise(function (resolve, reject) {
		let cnn = mysql.createConnection(dbase);
		
		cnn.connect(function (err) {
			let query = "SELECT CONCAT_WS('_', DATE_FORMAT(calldate, '%Y%m%d-%H%i%s'), src, "
				query+= "LOWER(resultado), REPLACE(destino, '/','_')) AS filename ";
				query+= `FROM vw_cdr WHERE uniqueid = '${uniqueid}'`;
				
			cnn.query(query, function (err, row) {
				cnn.end();

				if (err) return reject(err);
				
				let filename = (row.length ? row[0].filename : uniqueid) + ".WAV";
				
				return resolve(filename);
			});
		});
	});
}

/**
 * Recupera la lista de registros de un rango de fecha dada
 * 
 * @params {JSON} filter Objeto con el filtro a aplicar en la busqueda
 * @return {Promise} Retorno de promesa de la ejecucion
*/
function getCdrList (filter) {
	return new Promise(function (resolve, reject) {
		let rows = [];
		let query = "SELECT * FROM vw_cdr";
		
		if (!filter.isEmpty()) {
			let where = [];
			
			if (filter.fecha_inicio) {
				where.push(`calldate >= '${filter.fecha_inicio}'`);
		
				if (filter.fecha_termino)
					where.push(`calldate <= ADDDATE('${filter.fecha_termino}', 1)`);
			}
			
			if (where.length)
				query += " WHERE " + where.join(" AND ");
		}
		
		let cnn = mysql.createConnection(dbase);
		
		cnn.connect(function (err) {
			if (err) return reject(err);
			
			let result = cnn.query(query);
			
			result.on('result', function (row) {
				let file = `${monitorPath}/${row.uniqueid}.WAV`;
				cnn.pause();
				
				checkFile(file).then((isfile) => {
					let record = "";

					if (isfile)
						record = `<a class='btn green' href="/cdr?api=1&recording=${row.uniqueid}"><i class="material-icons">file_download</i></a>`;

					row.grabacion = record;
					rows.push(row);
					cnn.resume();
				}).catch((err) => {
					cnn.resume();
				});
			}).on('end', () => {
				cnn.end();
				resolve(rows);
			}).on('error', reject);
		});
	});
}

/**
 * Valida que la grabacion a recupara exista como archivo y le define
 * un nombre para la descarga
 * 
 * @param {String} Nombre del archivo (identificador de Asterisk) a recuperar
 * @return {Promise} Retorno de promesa con la validacion y el nombre del archivo a descargar
*/
function getRecording (file) {
	let fullpath = `${monitorPath}/${file}.WAV`;
	
	return checkFile(fullpath)
			.then((isfile) => {
				if (isfile)
					return getRecordName(file).then((filename) => {
						console.log("Filename: ", filename);
						return {
							fullpath: fullpath, 
							filename: filename
						};
					});
				else throw new Error("Recurso no es archivo");
			});
}

/**
 * Metodo publico que procesa el tipo de solicitud, la lista de registro,
 * la descarga de una grabacion o generar archivo CSV (TODO)
*/
module.exports = function (params) {
	if (params.download || params.recording) {
		if (params.recording && params.recording.length)
			return getRecording(params.recording);
		else return Promise.reject(new Error("Archivo no definido"));
	} else {
		return getCdrList(params);
	}
}
