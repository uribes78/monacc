/**
 * @script index.js
 * @author Saul Uribe <suribe@tico.com.mx>, <uribe.saul@yahoo.com.mx>
 * @date 27/Abr/2020
 * 
 * Proceso que permite generar un servicio Web para administrar un monitoreo
 * sobre un servicio Asterisk, el cual permite visualizar las actividades en
 * la(s) cola(s) que este tenga, permitiendo revisar en tiempo real su comportamiento
 * a traves de conexion WebSocket (Socket.IO)
 */
 
var app = require('express')();

const Asterisk = require('asterisk-manager');
const http = require('http');
const fs = require('fs');
const www = http.createServer(app);
const socket = require('socket.io')(www);
const { manager } = require('./config');

const port = process.env.PORT || 3000;
const Cdr = require('./module/cdr');
const Admin = require('./module/admin');

const pages = function (file) {
	let page = `${__dirname}/pages${file}.html`;
	return fs.createReadStream(page);
}

app.use(function (req, res, next) {
	let file = `${__dirname}${req.url}`;
	
	fs.stat(file, function (err, st) {
		if (err || !st.isFile()) {
			next();
		} else {
			res.status(200).type('js');
			fs.createReadStream(file).pipe(res);
		}
	});
});

app.get('/', function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	fs.createReadStream("index.html").pipe(res);
});

app.get('/cdr', function (req, res) {
	if (req.query.api) {
		Cdr(req.query).then((data) => {
			if (req.query.download || req.query.recording)
				return res.download(data.fullpath, data.filename);
			
			res.status(200).json(data).end();
		}).catch((err) => {
			res.status(500).json(err).end();
		});
	} else {
		res.status(200).type('html');
		pages(req.path).pipe(res);
	}
});

app.use(['/queues','/devices'], function (req, res) {
	//TODO: validar el tipo de solicitud y enviar al modulo como corresponda
	if (!req.query.list) {
		res.status(200).type('html');
		return pages(req.path).pipe(res)
	}

	Admin.main(req.path.substring(1), req.method, req.query || req.body)
		.then((rows) => {
			res.status(200).json(rows).end();
		}).catch((err) => {
			res.status(err.code).json(err).end();
		});
});

socket.on('connection', function (client) {
	requestQueueStatus(null, function (err) {
		if (err)
			return client.emit("AERROR", err);

		client.emit('queuestatuscomplete');
	});
	
	client.on('queuesummary', function (data, callback) {
		requestQueueSummary(data.queue, function (data) {
			if (data)
				client.emit(data.event.toLowerCase(), data);
			else callback();
		});
	});
	
	client.on('all members', function (data) {
		//Solicitar los miembros de la cola indicada
		requestQueueStatus(data.queue);
	});
});

var ami = new Asterisk(manager.port, manager.host, manager.user, manager.password, true);

/**
 * Valida el estado de una peer SIP en Asterisk
 * 
 * @param {String} peer Nombre del peer a revisar
 * @return {Promise} Respuesta de tipo 'promise' con el resultado
*/
var checkPeer = function (peer) {
	return new Promise(function (resolve, reject) {
		ami.action({
			action: "SIPshowpeer",
			peer: peer
		}, function (err, rt) {
			if (err) return reject(err);
			
			let result = {
				address: null, 
				online: (/OK\s\(\d+\s\w+\)/i.test(rt.status))
			};
			
			if (rt['address-ip'] != '(null)')
				result.address = rt['address-ip'];
				
			return resolve(result);
		})
	});
}

/**
 * Metodo que solicita la informacion de estado de una cola
 * o de todas en caos de no especificar
 * 
 * @param {String} queue Nombre o numero de la cola a consultar
 * @param {Function} callback Function de respuesta de ejecucion
 * @return void
*/
var requestQueueStatus = function (queue, callback) {
	if (!ami.isConnected())
		return callback(new Error("Asterisk connection not ready"));

	let data = {action: "QueueStatus"};

	/**
	 * Cuando se termina de revisar el estatus, enviar la respuesta
	 * de termino y se elimina el evento para evitar problemas de memoria
	 * 
	 * @return void
	*/
	let statusComplete = function () {
		ami.removeListener('queuestatuscomplete', statusComplete);
		if (typeof callback === "function")
			callback();
	};
	
	if (queue) data.queue = queue;
	
	
	ami.on('queuestatuscomplete', statusComplete);
	ami.action(data, function (err) {
		if (err) {
			console.error("Error checking queues status");
		}
	});
};

/**
 * Metodo que procesa la peticion de informacion de una cola
 * y sus estadisticas
 * 
 * @param {String} queue Nombre o numero de la cola consultar
 * @param {Function} callback Funcion de retorno de la ejecucion
 * @return void
*/
var requestQueueSummary = function (queue, callback) {
	if (!ami.isConnected())
		return callback(new Error("Servicio de telefonia desconectado"));
	
	let onqueuesummary = (data) => { callback(data) };
	
	ami.on('queuesummary', onqueuesummary);
	ami.once('queuesummarycomplete', function () {
		ami.removeListener('queuesummary', onqueuesummary);
		callback(null);
	});
	
	ami.action({
		action: "QueueSummary",
		queue: queue
	}, callback);
};

/**
 * Metodo parar procesar los eventos en escucha por parte
 * del servicio, notificando los mismos a los clientes conectados
 * 
 * @param {JSON} data Objeto con los datos el evento
 * @return void
*/
var onQueueEvent = function (data) {
	
	if (["queuemember", "queuememberstatus"].includes(data.event.toLowerCase())) {
		let peer = null;
		
		if (data.event.toLowerCase() === "queuememberstatus")
			peer = data.interface.split('/')[1];
		else peer = data.location.split('/')[1];
		
		checkPeer(peer).then((res) => {
			data.address = res.address;
			data.online = res.online;
			
			socket.emit(data.event.toLowerCase(), data);
		}).catch((err) => {
			console.error("onQueueEvent::%s: %s", data.event, err.message || err);
			socket.emit(data.event.toLowerCase(), data);
		});
		
		//Procesar llamadas abandonadas
	} else {
		socket.emit(data.event.toLowerCase(), data);
	}
};

/**
 * Procesar evento de conexion a Asterisk
 * una vez conectado se definen eventos necesarios
*/
ami.once('connect', function () {
	ami.keepConnected();
	//Una vez conectado, escuchar los eventos necesarios
	ami.on('queuememberstatus', onQueueEvent);
	ami.on('queuecallerjoin', onQueueEvent);
	ami.on('queuecallerleave', onQueueEvent);
	ami.on('queuecallerabandon', onQueueEvent);
	ami.on('queuemember', onQueueEvent);
	ami.on('queueentry', onQueueEvent);
	ami.on('agentconnect', onQueueEvent);
	ami.on('agentcomplete', onQueueEvent);
});

/**
 * Procesar evento de cierre de conexion eliminando
 * todos los eventos que pueda tener la instancia
*/
ami.on('close', function () {
	ami.removeAllListeners();
});

//Poner en escucha el servicio Web en el puerto dado
www.listen(port, function (err) {
	if (err) {
		console.error("Error listening.", err);
		process.exit(1);
	} else {
		console.log(`Servicio en escucha en *:${port}`);
	}
});
